#include "DS1302.h"

uint8 bdata		operData;
sbit bit_data0 = operData^0;
sbit bit_data7 = operData^7;

/*--------------------------------------------------------------------
函数名称：DS1302写一个字节
函数功能：
注意事项：无
提示说明：无
输    入：无
返    回：无
--------------------------------------------------------------------*/
void writeByte(void)
{
	uint8 i;

	for(i=0;i<=7;i++)
	{
		DS1302_SCK=0;
		DS1302_SIO=bit_data0;
		DS1302_SCK=1;
		operData=operData>>1;
	}
}
/*--------------------------------------------------------------------
函数名称：DS1302读一个字节
函数功能：
注意事项：无
提示说明：无
输    入：无
返    回：无
--------------------------------------------------------------------*/
void readByte(void)
{
	uint8 i;

	DS1302_SCK=1;
	for(i=0;i<=7;i++)
	{
		operData=operData>>1;
		DS1302_SCK=0;
		bit_data7=DS1302_SIO; 
		DS1302_SCK=1;
	}
}
/*--------------------------------------------------------------------
函数名称：DS1302充电参数
函数功能：
注意事项：无
提示说明：无
输    入：无
返    回：无
--------------------------------------------------------------------*/
void setChargePrmt(void)
{
	//DS1302_RST=0;
	DS1302_SCK=0;
	DS1302_RST=1;
	operData=0x90;
	writeByte();
	operData=0xA4;
	writeByte();
	DS1302_RST=0;
}
/*--------------------------------------------------------------------
函数名称：DS1302关写保护
函数功能：
注意事项：无
提示说明：无
输    入：无
返    回：无
--------------------------------------------------------------------*/
void closeWP(void)
{
	//DS1302_RST=0;
	DS1302_SCK=0;
	DS1302_RST=1;
	operData=0x8E;
	writeByte();
	operData=0x80;
	writeByte();
	DS1302_RST=0;			
}
/*--------------------------------------------------------------------
函数名称：DS1302开写保护
函数功能：
注意事项：无
提示说明：无
输    入：无
返    回：无
--------------------------------------------------------------------*/
void openWP(void)
{
	//DS1302_RST=0;
	DS1302_SCK=0;
	DS1302_RST=1;
	operData=0x8E;
	writeByte();
	operData=0x00;
	writeByte();
	DS1302_RST=0;
}
/*--------------------------------------------------------------------
函数名称：DS1302的一个完整写操作
函数功能：
注意事项：无
提示说明：无
输    入：
返    回：无
--------------------------------------------------------------------*/
void write(uint8 adr,uint8 dat)
{
	//DS1302_RST=0;   	
	DS1302_SCK=0;
	DS1302_RST=1;
	operData=0x80|(adr<<1);
	writeByte();
	operData=dat;
	writeByte();
	DS1302_RST=0;
}
/*--------------------------------------------------------------------
函数名称：DS1302的一个完整读操作
函数功能：
注意事项：无
提示说明：无
输    入：
返    回：无
--------------------------------------------------------------------*/
uint8 read(uint8 adr)
{
	//DS1302_RST=0;
	DS1302_SCK=0;
	DS1302_RST=1;
	operData=(adr<<1)|0x81;
	writeByte();
	readByte();
	DS1302_RST=0;
	return(operData); 
}
/*--------------------------------------------------------------------
函数名称：DS1302读时间
函数功能：
注意事项：无
提示说明：无
输    入：
返    回：无
--------------------------------------------------------------------*/
void DS1302_getTime(uint8 *buf)
{
	uint8 hourAdr=2,minuteAdr=1,secondAdr=0;

	buf[0]=read(secondAdr);
	buf[0]=changeHexToInt(buf[0]);

	buf[1]=read(minuteAdr);
	buf[1]=changeHexToInt(buf[1]);

	buf[2]=read(hourAdr);
	buf[2]=changeHexToInt(buf[2]);

	//DS1302_speaTime();
}
/*--------------------------------------------------------------------
函数名称：DS1302设置时间
函数功能：
注意事项：无
提示说明：无
输    入：
返    回：无
--------------------------------------------------------------------*/
void DS1302_setTime(uint8 hour,uint8 minute,uint8 second)
{
	uint8 hourAdr=2,minuteAdr=1,secondAdr=0;

	hour=changeIntToHex(hour);
	minute=changeIntToHex(minute);
	second=changeIntToHex(second);

	openWP();
	write(hourAdr,hour);
	write(minuteAdr,minute);
	write(secondAdr,second);
	closeWP();
}
/*--------------------------------------------------------------------
函数名称：DS1302初始化
函数功能：
注意事项：无
提示说明：无
输    入：无
返    回：无
--------------------------------------------------------------------*/
void DS1302_init(void)
{
	openWP();
	setChargePrmt();
	closeWP();
}
